# trusty-java8

Based on cenote/trusty-base

This Docker image is used to build java projects (ant, maven)

Installed software:

* oracle-java8-installer
* ant
* maven
